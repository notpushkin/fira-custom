TL;DR: nothing works yet

# Fira Custom

Pick only custom ligatures from Fira Code.

```
$ cat pick_ligatures.txt
# (See ligatures_list.txt for available ligatures)
# I really love this one and only need it, others are so duuuumb
w_w_w

$ python add_ligatures.py > myfont.ttx
$ ttx myfont.ttx
(outputs font in OTF... hopefully)
```
