import re
import sys
import json
import bs4

# TODO: read these from file?
UNITS_PER_EM = 1000
LIGATURES_FILENAME = "FiraCode-Regular-simple.ttx"
BASE_FILENAME = "base.ttx"
PICK_LIGATURES_FILENAME = "pick_ligatures.txt"


def read_ligatures():
    soup = bs4.BeautifulStoneSoup(open(LIGATURES_FILENAME))
    return {
        el["name"].split(".", 1)[0]: {
            "name": el["name"],
            "CharString": el,
            "mtx": soup.find("mtx", attrs={"name": el["name"]}),
        }
        for el in soup.find_all("CharString", attrs={"name": re.compile(r".*\.liga")})
    }


def tag(name, attrs={}):
    return bs4.Tag(name=name, attrs=attrs)


if __name__ == "__main__":
    pick_ligatures = [
        l
        for l in (l.strip() for l in open(PICK_LIGATURES_FILENAME).readlines())
        if l != "" and not l.startswith("#")
    ]

    ligatures = read_ligatures()
    print("Available ligatures:", ligatures.keys(), file=sys.stderr)

    base = bs4.BeautifulStoneSoup(open(BASE_FILENAME))

    el_Lookup = base.find(attrs={"firacustom-lookup": True})
    del el_Lookup["firacustom-lookup"]
    el_Lookup.append(tag("LookupFlag", {"value": "0"}))

    el_GlyphOrder = base.find("GlyphOrder")
    el_CharStrings = base.find("CharStrings")
    el_hmtx = base.find("hmtx")

    ligature_sets = {}
    el_LigatureSubst = tag("LigatureSubst", {"index": "0"})
    el_Lookup.append(el_LigatureSubst)

    for chars in pick_ligatures:
        ligature = ligatures[chars]

        fst, *rest = chars.split("_")
        if fst not in ligature_sets:
            ligature_sets[fst] = tag("LigatureSet", {"glyph": fst})
            el_LigatureSubst.append(ligature_sets[fst])

        ligature_sets[fst].append(tag("Ligature", {
            "components": ",".join(rest),
            "glyph": ligature["name"],
        }))

        el_GlyphOrder.append(tag("GlyphID", {
            "name": ligature["name"],
        }))
        el_CharStrings.append(ligature["CharString"])
        el_hmtx.append(ligature["mtx"])

    print(base)
